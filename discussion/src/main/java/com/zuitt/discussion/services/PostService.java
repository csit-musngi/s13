package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // create a post
    void createPost(Post post);

    // viewing all post
    Iterable<Post> getPosts();

    //delete a post
    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post post);

}
